Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :auth, only: %i[create]
      resources :categories, only: %i[index]
      resources :users, only: %i[index create update]
      resources :appointments, only: %i[index show create update destroy]
    end
  end

  namespace :admin do
    resources :doctors
  end
end
