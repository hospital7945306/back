
# Записи к врачу
class Appointment < ApplicationRecord
  # связь с таблицей пользователь для доктора
  belongs_to :doctor,
             class_name: 'User',
             foreign_key: :doctor_id

  # связь с таблицей пользователя для пациента
  belongs_to :patient,
             class_name: 'User',
             foreign_key: :patient_id

  # Методы для фильтрации
  def self.ransackable_attributes(_auth_object = nil)
    @ransackable_attributes ||= %w[recording_time]
  end

  def self.ransackable_associations(_auth_object = nil)
    @ransackable_associations ||= %w[doctor patient]
  end
end