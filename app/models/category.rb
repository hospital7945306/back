# категории врачей
class Category < ApplicationRecord
  # пользователи категорий
  has_many :users,
           class_name: 'User'

  def self.ransackable_attributes(_auth_object = nil)
    @ransackable_attributes ||= %w[name]
  end
end