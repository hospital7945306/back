# Пользователи
class User < ApplicationRecord
  # записи врача
  has_many :doctor_appointments,
           class_name: 'Appointment',
           dependent: :destroy,
           foreign_key: :doctor_id

  # записи пациента
  has_many :patient_appointments,
           class_name: 'Appointment',
           dependent: :destroy,
           foreign_key: :patient_id

  belongs_to :category, optional: true

  has_one_attached :avatar

  enum role: {doctor: 'doctor', patient: 'patient'}

  # методы фильтрации
  def self.ransackable_attributes(_auth_object = nil)
    @ransackable_attributes ||= %w[id first_name last_name middle_name role email category_id]
  end

  def self.ransackable_associations(auth_object = nil)
    ["avatar_attachment", "avatar_blob", "category", "doctor_appointments", "patient_appointments", "category"]
  end

end