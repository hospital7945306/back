# Базовый класс контролеера api
module Api
  class BaseController < ActionController::API
    include ErrorHanding
    require 'jwt'

    before_action :check_token
    before_action :require_auth

    def unsafe_params
      params.to_unsafe_hash.deep_symbolize_keys
    end

    # проверка токена авторизации
    def check_token
      raw_token = extract_token
      return false if raw_token.blank?

      data = JWT.decode raw_token, ENV['HMAC_SECRET'], 'HS256'

      user = User.find(data[0]['id'])

      @current_user = user
    end

    # Получение токена из параметров или заголовка
    def extract_token
      return false if params[:access_token].blank? && request.headers['Authorization'].blank?

      token_from_header = request.headers['Authorization'].split.last unless request.headers['Authorization'].blank?

      params[:access_token].presence || token_from_header
    end

    def require_auth
      render_errors(I18n.t('http_error.unauthorized'), status: :unauthorized) unless @current_user
    end
  end
end