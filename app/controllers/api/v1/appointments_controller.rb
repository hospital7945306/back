
# контроллер для усправления записью на прием
module Api
  module V1
    class AppointmentsController < BaseController
      before_action :appointment, only: [:show, :update, :destroy]

      # Получить все записи на прием с использованием фильтров
      def index
        scope = Appointment.eager_load(:doctor, :patient)
        q = scope.ransack(params[:q])

        q.sorts = 'recording_time DESC' if q.sorts.empty?

        render json: q.result, each_serializer: Api::V1::AppointmentSerializer
      end

      # Получить информацию о конкретной записи
      def show
        render json: @appointment, serializer: Api::V1::AppointmentSerializer
      end

      # Создать новую запись на прием
      def create
        appointment = Appointment.create!(appointment_params.merge(patient_id: @current_user.id))

        render json: appointment, serializer: Api::V1::AppointmentSerializer
      end

      # Обновить запись на прием
      def update
        @appointment.update!(appointment_params)

        render json: @appointment, serializer: Api::V1::AppointmentSerializer
      end

      # Отменить запись на прием
      def destroy
        @appointment.destroy!

        head :no_content
      end

      private

      def appointment
        @appointment = Appointment.find(params[:id])
      end

      def appointment_params
        params.permit(:recording_time, :doctor_id, :comment)
      end
    end
  end
end