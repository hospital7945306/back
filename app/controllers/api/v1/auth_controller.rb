# Контроллер для авторизации пользователя
module Api
  module V1
    class AuthController < BaseController
      # пропустить проверку токена авторизации
      skip_before_action :check_token
      skip_before_action :require_auth

      # Получить токен авторизации
      def create
        user = User.find_by!(email: params[:email], password: params[:password]).slice(:id, :last_name, :role)

        token = JWT.encode user, ENV['HMAC_SECRET'], 'HS256'

        render json: { token: token, user: user }
      end
    end
  end
end
