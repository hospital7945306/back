# Получение категорий
module Api
  module V1
    class CategoriesController < Api::BaseController
      # получить список категорий с фильтрами
      def index
        scope = Category.eager_load(:users)
        q = scope.ransack(params[:q])

        q.sorts = 'name ASC' if q.sorts.empty?

        render json: q.result, each_serializer: Api::V1::CategorySerializer
      end
    end
  end
end