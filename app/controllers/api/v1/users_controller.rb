# управление пользователями
module Api
  module V1
    class UsersController < BaseController
      # Вывести всех пользователей
      def index
        scope = User
        q = scope.ransack(params[:q])

        q.sorts = 'last_name ASC' if q.sorts.empty?

        render json: q.result, each_serializer: Api::V1::UserSerializer
      end

      def create
        user = User.create!(user_params)

        render json: user, serializer: Api::V1::UserSerializer
      end

      def update
        user = User.find(params[:id])

        user.update!(user_params)

        render json: user, serializer: Api::V1::UserSerializer

      end

      private

      def user_params
        params.permit(:last_name, :first_name, :middle_name, :email, :password).merge(role: 'patient')
      end
    end
  end
end