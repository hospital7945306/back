# frozen_string_literal: true

module Api
  module ErrorHanding
    extend ActiveSupport::Concern

    included do
      rescue_from ActiveRecord::RecordInvalid, with: :render_errors
      # see https://github.com/rails/rails/issues/43666
      rescue_from 'ActiveRecord::DeleteRestrictionError', with: :render_errors
      rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
      rescue_from ActiveRecord::NotNullViolation, with: :render_errors
      rescue_from KeyError, with: :render_errors
      rescue_from ActiveRecord::RecordNotUnique, with: :render_errors
      rescue_from JWT::VerificationError, with: :render_errors
    end

    private

    def render_errors(errors, status: 422)
      render json: { errors: Array(errors) }, status:
    end

    def record_not_found
      render_errors(I18n.t('http_error.forbidden'), status: :not_found)
    end
  end
end
