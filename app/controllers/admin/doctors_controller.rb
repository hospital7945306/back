module Admin
  class DoctorsController < ApplicationController
    before_action :doctor, only: [:edit, :update, :destroy]

    def index
      @doctors = User.where(role: 'doctor')
    end

    def new
      @doctor = User.new
    end

    def create

    end

    def edit

    end

    def update

    end

    def destroy

    end

    private

    def doctor
      @doctor = User.find(params[:id])
    end
  end
end
