module Api
  module V1
    # подготовка объекта категории для отправки через api

    class CategorySerializer < ActiveModel::Serializer
      attributes :id,
                 :name,
                 :doctors

      def doctors
        object.users
              .map { |user| { full_name: [user.last_name, user.first_name, user.middle_name].compact.join(' '), category: user.category.name } }
      end
    end
  end
end