module Api
  module V1
    # подготовка объекта пользователя для отправки через api
    class UserSerializer < ActiveModel::Serializer
      attributes :id,
                 :email,
                 :full_name

      def full_name
        [object.last_name, object.first_name, object.middle_name].compact.join(' ')
      end
    end
  end
end
