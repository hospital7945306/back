module Api
  module V1

    # подготовка объекта записи для отправки через api
    class AppointmentSerializer < ActiveModel::Serializer
      attributes :id,
                 :patient,
                 :doctor,
                 :comment,
                 :recording_time

      def doctor
        doctor = object.doctor

        { full_name: [doctor.last_name, doctor.first_name, doctor.middle_name].compact.join(' '), category: doctor.category.name }
      end

      def patient
        patient = object.patient

        { full_name: [patient.last_name, patient.first_name, patient.middle_name].compact.join(' ') }
      end
    end
  end
end
