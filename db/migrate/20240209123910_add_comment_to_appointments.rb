class AddCommentToAppointments < ActiveRecord::Migration[7.1]
  def change
    add_column :appointments, :comment, :string, default: '', null: true
  end
end
