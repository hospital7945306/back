class AddBaseRelations < ActiveRecord::Migration[7.1]
  def change
    create_table :categories do |t|
      t.timestamps

      t.string :name
    end

    create_table :users do |t|
      t.timestamps

      t.string :email, null: false
      t.string :password, null: false

      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :middle_name, null: true

      t.string :role, null: false, default: 'patient'

      t.references(:category,
                   type: :integer,
                   index: true,
                   null: true,
                   foreign_key: { to_table: :categories, on_delete: :nullify })
    end

    create_table :appointments do |t|
      t.timestamps

      t.timestamp :recording_time

      t.references(:doctor,
                   type: :integer,
                   index: true,
                   null: false,
                   foreign_key: { to_table: :users, on_delete: :cascade })

      t.references(:patient,
                   type: :integer,
                   index: true,
                   null: false,
                   foreign_key: { to_table: :users, on_delete: :cascade })
    end
  end
end
